using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OcApContracturlGetResponse.
    /// </summary>
    public class OcApContracturlGetResponse : TopResponse
    {
        /// <summary>
        /// 错误描述信息
        /// </summary>
        [XmlElement("error_description")]
        public string ErrorDescription { get; set; }

        /// <summary>
        /// 判断操作是否执行成功
        /// </summary>
        [XmlElement("is_success")]
        public bool IsSuccess { get; set; }

        /// <summary>
        /// 代扣url地址
        /// </summary>
        [XmlElement("url")]
        public string Url { get; set; }

    }
}
