using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallItemHscodeDetailGetResponse.
    /// </summary>
    public class TmallItemHscodeDetailGetResponse : TopResponse
    {
        /// <summary>
        /// 返回的计量单位和销售单位
        /// </summary>
        [XmlArray("results")]
        [XmlArrayItem("json")]
        public List<string> Results { get; set; }

    }
}
