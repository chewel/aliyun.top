using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// ItemCsellerAddResponse.
    /// </summary>
    public class ItemCsellerAddResponse : TopResponse
    {
        /// <summary>
        /// 新发布的商品
        /// </summary>
        [XmlElement("item")]
        public Top.Api.Domain.Item Item { get; set; }

    }
}
