using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// AstrolabeMappingGetResponse.
    /// </summary>
    public class AstrolabeMappingGetResponse : TopResponse
    {
        /// <summary>
        /// 卖家Id与授权AppKey的映射Mapping
        /// </summary>
        [XmlElement("result")]
        public string Result { get; set; }

    }
}
