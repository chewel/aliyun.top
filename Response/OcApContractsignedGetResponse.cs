using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OcApContractsignedGetResponse.
    /// </summary>
    public class OcApContractsignedGetResponse : TopResponse
    {
        /// <summary>
        /// 是否开通
        /// </summary>
        [XmlElement("contract_sign")]
        public bool ContractSign { get; set; }

        /// <summary>
        /// 调用出错描述信息
        /// </summary>
        [XmlElement("error_description")]
        public string ErrorDescription { get; set; }

    }
}
