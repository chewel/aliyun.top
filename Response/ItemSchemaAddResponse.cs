using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// ItemSchemaAddResponse.
    /// </summary>
    public class ItemSchemaAddResponse : TopResponse
    {
        /// <summary>
        /// 返回的结果
        /// </summary>
        [XmlElement("add_result")]
        public string AddResult { get; set; }

    }
}
