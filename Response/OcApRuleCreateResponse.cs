using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OcApRuleCreateResponse.
    /// </summary>
    public class OcApRuleCreateResponse : TopResponse
    {
        /// <summary>
        /// 错误描述
        /// </summary>
        [XmlElement("error_description")]
        public string ErrorDescription { get; set; }

        /// <summary>
        /// 表示方法是否正常执行成功
        /// </summary>
        [XmlElement("is_success")]
        public bool IsSuccess { get; set; }

        /// <summary>
        /// 规则id
        /// </summary>
        [XmlElement("rule_id")]
        public long RuleId { get; set; }

    }
}
