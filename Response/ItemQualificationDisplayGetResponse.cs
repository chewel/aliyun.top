using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// ItemQualificationDisplayGetResponse.
    /// </summary>
    public class ItemQualificationDisplayGetResponse : TopResponse
    {
        /// <summary>
        /// 返回资质采集配置
        /// </summary>
        [XmlElement("display_conf")]
        public Top.Api.Domain.DisplayQualifications DisplayConf { get; set; }

    }
}
