using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// ItemBsellerAddResponse.
    /// </summary>
    public class ItemBsellerAddResponse : TopResponse
    {
        /// <summary>
        /// 新发布的商品
        /// </summary>
        [XmlElement("item")]
        public Top.Api.Domain.Item Item { get; set; }

    }
}
