using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// QianniuKefuevalGetResponse.
    /// </summary>
    public class QianniuKefuevalGetResponse : TopResponse
    {
        /// <summary>
        /// resultCount
        /// </summary>
        [XmlElement("result_count")]
        public long ResultCount { get; set; }

        /// <summary>
        /// staffEvalDetails
        /// </summary>
        [XmlArray("staff_eval_details")]
        [XmlArrayItem("eval_detail")]
        public List<EvalDetailDomain> StaffEvalDetails { get; set; }

	/// <summary>
/// EvalDetailDomain Data Structure.
/// </summary>
[Serializable]

public class EvalDetailDomain : TopObject
{
	        /// <summary>
	        /// evalCode
	        /// </summary>
	        [XmlElement("eval_code")]
	        public long EvalCode { get; set; }
	
	        /// <summary>
	        /// evalRecer
	        /// </summary>
	        [XmlElement("eval_recer")]
	        public string EvalRecer { get; set; }
	
	        /// <summary>
	        /// evalSender
	        /// </summary>
	        [XmlElement("eval_sender")]
	        public string EvalSender { get; set; }
	
	        /// <summary>
	        /// evalTime
	        /// </summary>
	        [XmlElement("eval_time")]
	        public string EvalTime { get; set; }
	
	        /// <summary>
	        /// sendTime
	        /// </summary>
	        [XmlElement("send_time")]
	        public string SendTime { get; set; }
}

    }
}
