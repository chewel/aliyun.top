using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// WangwangEserviceChatrelationGetResponse.
    /// </summary>
    public class WangwangEserviceChatrelationGetResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ChatRelationResultDomain Result { get; set; }

	/// <summary>
/// ChatPeerDomain Data Structure.
/// </summary>
[Serializable]

public class ChatPeerDomain : TopObject
{
	        /// <summary>
	        /// 时间
	        /// </summary>
	        [XmlElement("date")]
	        public string Date { get; set; }
	
	        /// <summary>
	        /// 账号。长ID
	        /// </summary>
	        [XmlElement("uid")]
	        public string Uid { get; set; }
}

	/// <summary>
/// ChatRelationResultDomain Data Structure.
/// </summary>
[Serializable]

public class ChatRelationResultDomain : TopObject
{
	        /// <summary>
	        /// 返回码
	        /// </summary>
	        [XmlElement("code")]
	        public long Code { get; set; }
	
	        /// <summary>
	        /// 结束key。如果该值为空，则表示请求时间区间内的数据已经拿完。否则，表示区间内还有数据，可以将该值作为下次请求条件的page_beg传入进行迭代请求。
	        /// </summary>
	        [XmlElement("end_key")]
	        public string EndKey { get; set; }
	
	        /// <summary>
	        /// 关系列表
	        /// </summary>
	        [XmlArray("peers")]
	        [XmlArrayItem("chat_peer")]
	        public List<ChatPeerDomain> Peers { get; set; }
	
	        /// <summary>
	        /// 错误原因
	        /// </summary>
	        [XmlElement("reason")]
	        public string Reason { get; set; }
	
	        /// <summary>
	        /// 起始key。如果要实现上翻页，可以将该值作为下次请求的page_end
	        /// </summary>
	        [XmlElement("start_key")]
	        public string StartKey { get; set; }
}

    }
}
