using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TaeBillGetResponse.
    /// </summary>
    public class TaeBillGetResponse : TopResponse
    {
        /// <summary>
        /// 账单明细
        /// </summary>
        [XmlElement("bill")]
        public Top.Api.Domain.BillDto Bill { get; set; }

    }
}
