using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// ItemSchemaUpdateResponse.
    /// </summary>
    public class ItemSchemaUpdateResponse : TopResponse
    {
        /// <summary>
        /// 编辑商品返回的结果信息，暂时只返回 itemId
        /// </summary>
        [XmlElement("update_result")]
        public string UpdateResult { get; set; }

    }
}
