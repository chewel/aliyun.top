using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OcOrderCreateResponse.
    /// </summary>
    public class OcOrderCreateResponse : TopResponse
    {
        /// <summary>
        /// 执行失败原因
        /// </summary>
        [XmlElement("error_description")]
        public string ErrorDescription { get; set; }

        /// <summary>
        /// 表示是否执行成功
        /// </summary>
        [XmlElement("is_success")]
        public bool IsSuccess { get; set; }

        /// <summary>
        /// OcOrder
        /// </summary>
        [XmlElement("oc_order")]
        public Top.Api.Domain.OcOrder OcOrder { get; set; }

    }
}
