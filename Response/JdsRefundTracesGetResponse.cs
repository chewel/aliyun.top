using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// JdsRefundTracesGetResponse.
    /// </summary>
    public class JdsRefundTracesGetResponse : TopResponse
    {
        /// <summary>
        /// 退款跟踪列表
        /// </summary>
        [XmlArray("traces")]
        [XmlArrayItem("refund_trace")]
        public List<Top.Api.Domain.RefundTrace> Traces { get; set; }

        /// <summary>
        /// 用户在全链路系统中的状态(比如是否存在)
        /// </summary>
        [XmlElement("user_status")]
        public string UserStatus { get; set; }

    }
}
