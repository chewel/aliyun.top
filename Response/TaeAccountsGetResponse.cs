using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TaeAccountsGetResponse.
    /// </summary>
    public class TaeAccountsGetResponse : TopResponse
    {
        /// <summary>
        /// 返回的科目信息
        /// </summary>
        [XmlArray("accounts")]
        [XmlArrayItem("top_account_dto")]
        public List<Top.Api.Domain.TopAccountDto> Accounts { get; set; }

        /// <summary>
        /// 返回记录行数
        /// </summary>
        [XmlElement("total_results")]
        public long TotalResults { get; set; }

    }
}
