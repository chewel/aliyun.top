using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OcOrderApUpdateResponse.
    /// </summary>
    public class OcOrderApUpdateResponse : TopResponse
    {
        /// <summary>
        /// 执行失败时候的错误描述信息
        /// </summary>
        [XmlElement("error_description")]
        public string ErrorDescription { get; set; }

        /// <summary>
        /// 描述操作执行是否成功
        /// </summary>
        [XmlElement("is_success")]
        public bool IsSuccess { get; set; }

        /// <summary>
        /// OC订单id
        /// </summary>
        [XmlElement("oc_order_id")]
        public long OcOrderId { get; set; }

    }
}
