using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TopatsJushitaJdpDatadeleteResponse.
    /// </summary>
    public class TopatsJushitaJdpDatadeleteResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDoDomain Result { get; set; }

	/// <summary>
/// ResultDoDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDoDomain : TopObject
{
	        /// <summary>
	        /// errCode
	        /// </summary>
	        [XmlElement("err_code")]
	        public string ErrCode { get; set; }
	
	        /// <summary>
	        /// errTrace
	        /// </summary>
	        [XmlElement("err_trace")]
	        public string ErrTrace { get; set; }
	
	        /// <summary>
	        /// result
	        /// </summary>
	        [XmlElement("result")]
	        public string Result { get; set; }
}

    }
}
