using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallItemEduUpdateOutlineResponse.
    /// </summary>
    public class TmallItemEduUpdateOutlineResponse : TopResponse
    {
        /// <summary>
        /// error
        /// </summary>
        [XmlElement("error")]
        public bool Error { get; set; }

        /// <summary>
        /// errorCode
        /// </summary>
        [XmlElement("errorcode")]
        public string Errorcode { get; set; }

        /// <summary>
        /// errorMsg
        /// </summary>
        [XmlElement("errormsg")]
        public string Errormsg { get; set; }

        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public string Result { get; set; }

    }
}
