using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TaeBookBillsGetResponse.
    /// </summary>
    public class TaeBookBillsGetResponse : TopResponse
    {
        /// <summary>
        /// 虚拟账户账单列表
        /// </summary>
        [XmlArray("bills")]
        [XmlArrayItem("top_acct_cash_jour_dto")]
        public List<Top.Api.Domain.TopAcctCashJourDto> Bills { get; set; }

        /// <summary>
        /// 是否有下一页
        /// </summary>
        [XmlElement("has_next")]
        public bool HasNext { get; set; }

        /// <summary>
        /// 当前查询的结果数,非总数
        /// </summary>
        [XmlElement("total_results")]
        public long TotalResults { get; set; }

    }
}
