using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TaeBookBillGetResponse.
    /// </summary>
    public class TaeBookBillGetResponse : TopResponse
    {
        /// <summary>
        /// 虚拟账户账单
        /// </summary>
        [XmlElement("bookbill")]
        public Top.Api.Domain.TopAcctCashJourDto Bookbill { get; set; }

    }
}
