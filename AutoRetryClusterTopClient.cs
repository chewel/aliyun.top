﻿using Top.Api.Cluster;

namespace Top.Api
{
    public class AutoRetryClusterTopClient : AutoRetryTopClient
    {
        public AutoRetryClusterTopClient(string serverUrl, string appKey, string appSecret)
            : base(serverUrl, appKey, appSecret)
        {
            ClusterManager.InitRefreshThread(this);
        }

        public AutoRetryClusterTopClient(string serverUrl, string appKey, string appSecret, string format)
            : base(serverUrl, appKey, appSecret, format)
        {
            ClusterManager.InitRefreshThread(this);
        }

        internal override string GetServerUrl(string serverUrl, string apiName, string session)
        {
            var dnsConfig = ClusterManager.GetDnsConfigFromCache();
            return dnsConfig == null ? serverUrl : dnsConfig.GetBestVipUrl(serverUrl, apiName, session);
        }

        internal override string GetSdkVersion()
        {
            return Constants.SDK_VERSION_CLUSTER;
        }
    }
}
