namespace Qimen.Api.Request
{
    /// <summary>
    /// TOP API: taobao.qimen.transferorder.query
    /// </summary>
    public class TransferorderQueryRequest : QimenRequest<Response.TransferorderQueryResponse>
    {
        /// <summary>
        /// ERP单号
        /// </summary>
        public string ErpOrderCode { get; set; }

        /// <summary>
        /// 111
        /// </summary>
        public string OwnerCode { get; set; }

        /// <summary>
        /// 调拨单号
        /// </summary>
        public string TransferOrderCode { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.qimen.transferorder.query";
        }


        #endregion
    }
}
