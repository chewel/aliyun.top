namespace Qimen.Api.Request
{
    /// <summary>
    /// TOP API: taobao.qimen.order.callback
    /// </summary>
    public class OrderCallbackRequest : QimenRequest<Response.OrderCallbackResponse>
    {
        /// <summary>
        /// 奇门仓储字段,C123,string(50),,
        /// </summary>
        public string DeliveryOrderCode { get; set; }

        /// <summary>
        /// 运单号
        /// </summary>
        public string ExpressCode { get; set; }

        /// <summary>
        /// 奇门仓储字段,C123,string(50),,
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 奇门仓储字段,C123,string(50),,
        /// </summary>
        public string OwnerCode { get; set; }

        /// <summary>
        /// 奇门仓储字段,C123,string(50),,
        /// </summary>
        public string WarehouseCode { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.qimen.order.callback";
        }


        #endregion
    }
}
