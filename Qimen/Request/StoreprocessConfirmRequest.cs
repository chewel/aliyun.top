using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Qimen.Api.Request
{
    /// <summary>
    /// TOP API: taobao.qimen.storeprocess.confirm
    /// </summary>
    [XmlRoot("request")]
    public class StoreprocessConfirmRequest : QimenRequest<Response.StoreprocessConfirmResponse>
    {
        /// <summary>
        /// 实际作业总数量
        /// </summary>
        [XmlElement("actualQty")]
        public long? ActualQty { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        [XmlElement("extendProps")]
        public string ExtendProps { get; set; }

        [XmlArray("materialitems")]
        [XmlArrayItem("materialitem")]
        public List<MaterialItemDomain> Materialitems { get; set; }

        /// <summary>
        /// 加工单完成时间(YYYY-MM-DD HH:MM:SS)
        /// </summary>
        [XmlElement("orderCompleteTime")]
        public string OrderCompleteTime { get; set; }

        /// <summary>
        /// 单据类型(CNJG=仓内加工作业单)
        /// </summary>
        [XmlElement("orderType")]
        public string OrderType { get; set; }

        /// <summary>
        /// 外部业务编码(一个合作伙伴中要求唯一多次确认时;每次传入要求唯一(一般传入WMS损益单据编码))
        /// </summary>
        [XmlElement("outBizCode")]
        public string OutBizCode { get; set; }

        /// <summary>
        /// 货主编码
        /// </summary>
        [XmlElement("ownerCode")]
        public string OwnerCode { get; set; }

        /// <summary>
        /// 加工单编码
        /// </summary>
        [XmlElement("processOrderCode")]
        public string ProcessOrderCode { get; set; }

        /// <summary>
        /// 仓储系统加工单ID
        /// </summary>
        [XmlElement("processOrderId")]
        public string ProcessOrderId { get; set; }

        [XmlArray("productitems")]
        [XmlArrayItem("productitem")]
        public List<ProductItemDomain> Productitems { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [XmlElement("remark")]
        public string Remark { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.qimen.storeprocess.confirm";
        }


	/// <summary>
/// MaterialItemDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("item")]
public class MaterialItemDomain
{
	        /// <summary>
	        /// 批次编码
	        /// </summary>
	        [XmlElement("batchCode")]
	        public string BatchCode { get; set; }
	
	        /// <summary>
	        /// 商品过期日期(YYYY-MM-DD)
	        /// </summary>
	        [XmlElement("expireDate")]
	        public string ExpireDate { get; set; }
	
	        /// <summary>
	        /// 库存类型(ZP=正品;CC=残次;JS=机损;XS=箱损;默认为ZP)
	        /// </summary>
	        [XmlElement("inventoryType")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// erp系统商品编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 仓储系统商品ID
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// ownerCode
	        /// </summary>
	        [XmlElement("ownerCode")]
	        public string OwnerCode { get; set; }
	
	        /// <summary>
	        /// 生产批号
	        /// </summary>
	        [XmlElement("produceCode")]
	        public string ProduceCode { get; set; }
	
	        /// <summary>
	        /// 商品生产日期(YYYY-MM-DD)
	        /// </summary>
	        [XmlElement("productDate")]
	        public string ProductDate { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public Nullable<long> Quantity { get; set; }
	
	        /// <summary>
	        /// ratioQty
	        /// </summary>
	        [XmlElement("ratioQty")]
	        public string RatioQty { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
}

	/// <summary>
/// ProductItemDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("item")]
public class ProductItemDomain
{
	        /// <summary>
	        /// 批次编码
	        /// </summary>
	        [XmlElement("batchCode")]
	        public string BatchCode { get; set; }
	
	        /// <summary>
	        /// 商品过期日期(YYYY-MM-DD)
	        /// </summary>
	        [XmlElement("expireDate")]
	        public string ExpireDate { get; set; }
	
	        /// <summary>
	        /// 库存类型(ZP=正品;CC=残次;JS=机损;XS=箱损;默认为ZP)
	        /// </summary>
	        [XmlElement("inventoryType")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// erp系统商品编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 仓储系统商品ID
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// ownerCode
	        /// </summary>
	        [XmlElement("ownerCode")]
	        public string OwnerCode { get; set; }
	
	        /// <summary>
	        /// 生产批号
	        /// </summary>
	        [XmlElement("produceCode")]
	        public string ProduceCode { get; set; }
	
	        /// <summary>
	        /// 商品生产日期(YYYY-MM-DD)
	        /// </summary>
	        [XmlElement("productDate")]
	        public string ProductDate { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public Nullable<long> Quantity { get; set; }
	
	        /// <summary>
	        /// ratioQty
	        /// </summary>
	        [XmlElement("ratioQty")]
	        public string RatioQty { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
}

        #endregion
    }
}
