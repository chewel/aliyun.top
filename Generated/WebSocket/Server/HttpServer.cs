#region MIT License
/**
 * HttpServer.cs
 *
 * The MIT License
 *
 * Copyright (c) 2012 sta.blockhead
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Microsoft.Extensions.Configuration;
using WebSocketSharp.Net;

namespace WebSocketSharp.Server
{
    public class HttpServer
    {
        #region Fields

        private Thread _acceptRequestThread;
        private bool _isWindows;
        private HttpListener _listener;
        private string _rootPath;
        private ServiceManager _services;
        private readonly IConfiguration _configuration;

        #endregion

        #region Constructors

        public HttpServer()
            : this(80)
        {
        }

        public HttpServer(int port)
        {
            Port = port;
            _configuration = new ConfigurationBuilder().SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json").Build();
            init();
        }

        #endregion

        #region Properties

        public int Port { get; }

        public IEnumerable<string> ServicePath => _services.Path;

        public bool Sweeped
        {
            get => _services.Sweeped;
            set => _services.Sweeped = value;
        }

        #endregion

        #region Events

        public event EventHandler<ResponseEventArgs> OnConnect;
        public event EventHandler<ResponseEventArgs> OnDelete;
        public event EventHandler<ErrorEventArgs> OnError;
        public event EventHandler<ResponseEventArgs> OnGet;
        public event EventHandler<ResponseEventArgs> OnHead;
        public event EventHandler<ResponseEventArgs> OnOptions;
        public event EventHandler<ResponseEventArgs> OnPatch;
        public event EventHandler<ResponseEventArgs> OnPost;
        public event EventHandler<ResponseEventArgs> OnPut;
        public event EventHandler<ResponseEventArgs> OnTrace;

        #endregion

        #region Private Methods

        private void acceptRequest()
        {
            while (true)
            {
                try
                {
                    var context = _listener.GetContext();
                    respondAsync(context);
                }
                catch (HttpListenerException)
                {
                    // HttpListener has been closed.
                    break;
                }
                catch (Exception ex)
                {
                    onError(ex.Message);
                    break;
                }
            }
        }

        private void configureFromConfigFile()
        {
            _rootPath = _configuration["RootPath"];
        }

        private void init()
        {
            _isWindows = false;
            _listener = new HttpListener();
            _services = new ServiceManager();

            var os = Environment.OSVersion;
            if (os.Platform != PlatformID.Unix && os.Platform != PlatformID.MacOSX)
                _isWindows = true;

            var prefix = string.Format(
              "http{0}://*:{1}/", Port == 443 ? "s" : String.Empty, Port);
            _listener.Prefixes.Add(prefix);

            configureFromConfigFile();
        }

        private bool isUpgrade(HttpListenerRequest request, string value)
        {
            return Ext.Exists(request.Headers, "Upgrade", value) && Ext.Exists(request.Headers, "Connection", "Upgrade");
        }

        private void onError(string message)
        {
#if DEBUG
            var callerFrame = new StackFrame(1);
            var caller = callerFrame.GetMethod();
            Console.WriteLine("HTTPSV: Error@{0}: {1}", caller.Name, message);
#endif
            Ext.Emit(OnError, this, new ErrorEventArgs(message));
        }

        private void respond(HttpListenerContext context)
        {
            var req = context.Request;
            var res = context.Response;
            var eventArgs = new ResponseEventArgs(context);

            switch (req.HttpMethod)
            {
                case "GET" when OnGet != null:
                    OnGet(this, eventArgs);
                    return;
                case "HEAD" when OnHead != null:
                    OnHead(this, eventArgs);
                    return;
                case "POST" when OnPost != null:
                    OnPost(this, eventArgs);
                    return;
                case "PUT" when OnPut != null:
                    OnPut(this, eventArgs);
                    return;
                case "DELETE" when OnDelete != null:
                    OnDelete(this, eventArgs);
                    return;
                case "OPTIONS" when OnOptions != null:
                    OnOptions(this, eventArgs);
                    return;
                case "TRACE" when OnTrace != null:
                    OnTrace(this, eventArgs);
                    return;
                case "CONNECT" when OnConnect != null:
                    OnConnect(this, eventArgs);
                    return;
                case "PATCH" when OnPatch != null:
                    OnPatch(this, eventArgs);
                    return;
            }

            res.StatusCode = (int)HttpStatusCode.NotImplemented;
        }

        private void respondAsync(HttpListenerContext context)
        {
            void RespondCb(object state)
            {
                var req = context.Request;
                var res = context.Response;

                try
                {
                    if (isUpgrade(req, "websocket"))
                    {
                        if (upgradeToWebSocket(context))
                            return;
                    }
                    else
                    {
                        respond(context);
                    }

                    res.Close();
                }
                catch (Exception ex)
                {
                    onError(ex.Message);
                }
            }

            ThreadPool.QueueUserWorkItem(RespondCb);
        }

        private void startAcceptRequestThread()
        {
            _acceptRequestThread = new Thread(new ThreadStart(acceptRequest));
            _acceptRequestThread.IsBackground = true;
            _acceptRequestThread.Start();
        }

        private bool upgradeToWebSocket(HttpListenerContext context)
        {
            var res = context.Response;
            var wsContext = context.AcceptWebSocket();
            var socket = wsContext.WebSocket;
            var path = Ext.UrlDecode(wsContext.Path);

            if (!_services.TryGetServiceHost(path, out var svcHost))
            {
                res.StatusCode = (int)HttpStatusCode.NotImplemented;
                return false;
            }

            svcHost.BindWebSocket(socket);
            return true;
        }

        #endregion

        #region Public Methods

        public void AddService<T>(string absPath)
          where T : WebSocketService, new()
        {
            if (!Ext.IsValidAbsolutePath(absPath, out var msg))
            {
                onError(msg);
                return;
            }

            var svcHost = new WebSocketServiceHost<T>();
            svcHost.Uri = Ext.ToUri(absPath);
            if (!Sweeped)
                svcHost.Sweeped = Sweeped;

            _services.Add(absPath, svcHost);
        }

        public byte[] GetFile(string path)
        {
            var filePath = _rootPath + path;
            if (_isWindows)
                filePath = filePath.Replace("/", "\\");

            return File.Exists(filePath) ? File.ReadAllBytes(filePath) : null;
        }

        public void Start()
        {
            _listener.Start();
            startAcceptRequestThread();
        }

        public void Stop()
        {
            _listener.Close();
            _acceptRequestThread.Join(5 * 1000);
            _services.Stop();
        }

        #endregion
    }
}