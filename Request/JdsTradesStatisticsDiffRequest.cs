using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.jds.trades.statistics.diff
    /// </summary>
    public class JdsTradesStatisticsDiffRequest : BaseTopRequest<Top.Api.Response.JdsTradesStatisticsDiffResponse>
    {
        /// <summary>
        /// 查询的日期，格式如YYYYMMDD的日期对应的数字
        /// </summary>
        public Nullable<long> Date { get; set; }

        /// <summary>
        /// 页数
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 需要比较的状态
        /// </summary>
        public string PostStatus { get; set; }

        /// <summary>
        /// 需要比较的状态，将会和post_status做比较
        /// </summary>
        public string PreStatus { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.jds.trades.statistics.diff";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("date", this.Date);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("post_status", this.PostStatus);
            parameters.Add("pre_status", this.PreStatus);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("date", this.Date);
            RequestValidator.ValidateRequired("post_status", this.PostStatus);
            RequestValidator.ValidateRequired("pre_status", this.PreStatus);
        }

        #endregion
    }
}
