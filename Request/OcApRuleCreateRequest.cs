using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.oc.ap.rule.create
    /// </summary>
    public class OcApRuleCreateRequest : BaseTopRequest<Top.Api.Response.OcApRuleCreateResponse>
    {
        /// <summary>
        /// 传入比例数组后者金额数组
        /// </summary>
        public string DivisionRule { get; set; }

        /// <summary>
        /// 规则描述相关扩展信息，divisonRule的值包含（"byAmount" 或者 "byPercentage"），excutionPeriod的值包含（ "month" 或者 "day" 或者 "now"）
        /// </summary>
        public string ExtAttributes { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.oc.ap.rule.create";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("division_rule", this.DivisionRule);
            parameters.Add("ext_attributes", this.ExtAttributes);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("division_rule", this.DivisionRule);
            RequestValidator.ValidateMaxListSize("division_rule", this.DivisionRule, 20);
            RequestValidator.ValidateRequired("ext_attributes", this.ExtAttributes);
        }

        #endregion
    }
}
