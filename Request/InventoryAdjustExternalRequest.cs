using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.inventory.adjust.external
    /// </summary>
    public class InventoryAdjustExternalRequest : BaseTopRequest<Top.Api.Response.InventoryAdjustExternalResponse>
    {
        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.inventory.adjust.external";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
