using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.jds.hluser.get
    /// </summary>
    public class JdsHluserGetRequest : BaseTopRequest<Top.Api.Response.JdsHluserGetResponse>
    {
        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.jds.hluser.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
