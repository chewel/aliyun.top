using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.wangwang.abstract.deleteword
    /// </summary>
    public class WangwangAbstractDeletewordRequest : BaseTopRequest<Top.Api.Response.WangwangAbstractDeletewordResponse>
    {
        /// <summary>
        /// 传入参数的字符集
        /// </summary>
        public string Charset { get; set; }

        /// <summary>
        /// 关键词，长度大于0
        /// </summary>
        public string Word { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.wangwang.abstract.deleteword";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("charset", this.Charset);
            parameters.Add("word", this.Word);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("word", this.Word);
            RequestValidator.ValidateMaxLength("word", this.Word, 12);
        }

        #endregion
    }
}
