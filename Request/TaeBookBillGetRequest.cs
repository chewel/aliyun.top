using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.tae.book.bill.get
    /// </summary>
    public class TaeBookBillGetRequest : BaseTopRequest<Top.Api.Response.TaeBookBillGetResponse>
    {
        /// <summary>
        /// 虚拟账户科目编号
        /// </summary>
        public Nullable<long> AccountId { get; set; }

        /// <summary>
        /// 虚拟账户流水编号
        /// </summary>
        public Nullable<long> Bid { get; set; }

        /// <summary>
        /// 需要返回的字段:参见BookBill结构体
        /// </summary>
        public string Fields { get; set; }

        /// <summary>
        /// 虚拟账户流水编号
        /// </summary>
        public Nullable<long> Id { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.tae.book.bill.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("account_id", this.AccountId);
            parameters.Add("bid", this.Bid);
            parameters.Add("fields", this.Fields);
            parameters.Add("id", this.Id);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("account_id", this.AccountId);
            RequestValidator.ValidateRequired("fields", this.Fields);
            RequestValidator.ValidateMaxListSize("fields", this.Fields, 20);
            RequestValidator.ValidateRequired("id", this.Id);
        }

        #endregion
    }
}
