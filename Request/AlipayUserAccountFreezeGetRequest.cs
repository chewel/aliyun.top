using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: alipay.user.account.freeze.get
    /// </summary>
    public class AlipayUserAccountFreezeGetRequest : BaseTopRequest<Top.Api.Response.AlipayUserAccountFreezeGetResponse>
    {
        /// <summary>
        /// 冻结类型，多个用,分隔。不传返回所有类型的冻结金额。  DEPOSIT_FREEZE,充值冻结  WITHDRAW_FREEZE,提现冻结  PAYMENT_FREEZE,支付冻结  BAIL_FREEZE,保证金冻结  CHARGE_FREEZE,收费冻结  PRE_DEPOSIT_FREEZE,预存款冻结  LOAN_FREEZE,贷款冻结  OTHER_FREEZE,其它
        /// </summary>
        public string FreezeType { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "alipay.user.account.freeze.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("freeze_type", this.FreezeType);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
