using System;
using System.Xml.Serialization;
using Top.Api.Domain;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.oc.order.create
    /// </summary>
    public class OcOrderCreateRequest : BaseTopRequest<Top.Api.Response.OcOrderCreateResponse>
    {
        /// <summary>
        /// OC订单
        /// </summary>
        public string ParamOCOrder { get; set; }

        public OcOrder ParamOCOrder_ { set { this.ParamOCOrder = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.oc.order.create";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param_o_c_order", this.ParamOCOrder);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param_o_c_order", this.ParamOCOrder);
        }

        #endregion
    }
}
