using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.jds.hluser.update
    /// </summary>
    public class JdsHluserUpdateRequest : BaseTopRequest<Top.Api.Response.JdsHluserUpdateResponse>
    {
        /// <summary>
        /// 回流信息是否开通买家端展示,可选值open,close
        /// </summary>
        public string OpenForBuyer { get; set; }

        /// <summary>
        /// 为空,则默认是X_TO_SYSTEM,X_WAIT_ALLOCATION,X_OUT_WAREHOUSE 可选值是X_DOWNLOADED,X_TO_SYSTEM,X_SERVICE_AUDITED,X_FINANCE_AUDITED,X_ALLOCATION_NOTIFIED,X_WAIT_ALLOCATION,X_SORT_PRINTED,X_SEND_PRINTED,X_LOGISTICS_PRINTED,X_SORTED,X_EXAMINED,X_PACKAGED,X_WEIGHED,X_OUT_WAREHOUS
        /// </summary>
        public string OpenNodes { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.jds.hluser.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("open_for_buyer", this.OpenForBuyer);
            parameters.Add("open_nodes", this.OpenNodes);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
