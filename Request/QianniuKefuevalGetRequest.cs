using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.qianniu.kefueval.get
    /// </summary>
    public class QianniuKefuevalGetRequest : BaseTopRequest<Top.Api.Response.QianniuKefuevalGetResponse>
    {
        /// <summary>
        /// 开始时间，格式yyyyMMddHHmmss
        /// </summary>
        public string Btime { get; set; }

        /// <summary>
        /// 结束时间,格式yyyyMMddHHmmss
        /// </summary>
        public string Etime { get; set; }

        /// <summary>
        /// 客服的nick，多个用逗号分隔，不要超过10个，带cntaobao的长nick
        /// </summary>
        public string QueryIds { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.qianniu.kefueval.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("btime", this.Btime);
            parameters.Add("etime", this.Etime);
            parameters.Add("query_ids", this.QueryIds);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("btime", this.Btime);
            RequestValidator.ValidateRequired("etime", this.Etime);
            RequestValidator.ValidateRequired("query_ids", this.QueryIds);
        }

        #endregion
    }
}
