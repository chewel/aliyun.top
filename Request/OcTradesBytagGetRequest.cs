using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.oc.trades.bytag.get
    /// </summary>
    public class OcTradesBytagGetRequest : BaseTopRequest<Top.Api.Response.OcTradesBytagGetResponse>
    {
        /// <summary>
        /// 当前页
        /// </summary>
        public Nullable<long> Page { get; set; }

        /// <summary>
        /// 分页大小
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 标签名称
        /// </summary>
        public string TagName { get; set; }

        /// <summary>
        /// 标签类型，1官方，2自定义
        /// </summary>
        public Nullable<long> TagType { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.oc.trades.bytag.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("page", this.Page);
            parameters.Add("page_size", this.PageSize);
            parameters.Add("tag_name", this.TagName);
            parameters.Add("tag_type", this.TagType);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("tag_name", this.TagName);
            RequestValidator.ValidateRequired("tag_type", this.TagType);
        }

        #endregion
    }
}
