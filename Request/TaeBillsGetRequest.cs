using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.tae.bills.get
    /// </summary>
    public class TaeBillsGetRequest : BaseTopRequest<Top.Api.Response.TaeBillsGetResponse>
    {
        /// <summary>
        /// 页数,建议不要超过100页,越大性能越低,有可能会超时
        /// </summary>
        public Nullable<long> CurrentPage { get; set; }

        /// <summary>
        /// 传入需要返回的字段,参见Bill结构体
        /// </summary>
        public string Fields { get; set; }

        /// <summary>
        /// 科目编号
        /// </summary>
        public Nullable<long> ItemId { get; set; }

        /// <summary>
        /// 交易编号
        /// </summary>
        public Nullable<long> PTradeId { get; set; }

        /// <summary>
        /// 每页大小,默认40条,可选范围 ：40~100
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 查询条件中的时间类型:1-交易订单完成时间biz_time 2-支付宝扣款时间pay_time 如果不填默认为2即根据支付时间查询,查询的结果会根据该时间倒排序
        /// </summary>
        public Nullable<long> QueryDateType { get; set; }

        /// <summary>
        /// 结束时间,限制:结束时间-开始时间不能大于1天(根据order_id或者trade_id查询除外)
        /// </summary>
        public Nullable<DateTime> QueryEndDate { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public Nullable<DateTime> QueryStartDate { get; set; }

        /// <summary>
        /// 子订单编号
        /// </summary>
        public Nullable<long> TradeId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.tae.bills.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("current_page", this.CurrentPage);
            parameters.Add("fields", this.Fields);
            parameters.Add("item_id", this.ItemId);
            parameters.Add("p_trade_id", this.PTradeId);
            parameters.Add("page_size", this.PageSize);
            parameters.Add("query_date_type", this.QueryDateType);
            parameters.Add("query_end_date", this.QueryEndDate);
            parameters.Add("query_start_date", this.QueryStartDate);
            parameters.Add("trade_id", this.TradeId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("fields", this.Fields);
            RequestValidator.ValidateMaxListSize("fields", this.Fields, 20);
            RequestValidator.ValidateRequired("query_end_date", this.QueryEndDate);
            RequestValidator.ValidateRequired("query_start_date", this.QueryStartDate);
        }

        #endregion
    }
}
