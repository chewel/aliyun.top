using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.item.hscode.detail.get
    /// </summary>
    public class TmallItemHscodeDetailGetRequest : BaseTopRequest<Top.Api.Response.TmallItemHscodeDetailGetResponse>
    {
        /// <summary>
        /// hscode
        /// </summary>
        public string Hscode { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.item.hscode.detail.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("hscode", this.Hscode);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("hscode", this.Hscode);
        }

        #endregion
    }
}
