using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.item.qualification.display.get
    /// </summary>
    public class ItemQualificationDisplayGetRequest : BaseTopRequest<Top.Api.Response.ItemQualificationDisplayGetResponse>
    {
        /// <summary>
        /// 类目id
        /// </summary>
        public Nullable<long> CategoryId { get; set; }

        /// <summary>
        /// 商品id
        /// </summary>
        public Nullable<long> ItemId { get; set; }

        /// <summary>
        /// 参数列表，为key和value都是string的map的转化的json格式
        /// </summary>
        public string Param { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.item.qualification.display.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("category_id", this.CategoryId);
            parameters.Add("item_id", this.ItemId);
            parameters.Add("param", this.Param);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
