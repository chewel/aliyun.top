using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.oc.order.ap.update
    /// </summary>
    public class OcOrderApUpdateRequest : BaseTopRequest<Top.Api.Response.OcOrderApUpdateResponse>
    {
        /// <summary>
        /// 调用创建OC订单接口生成的id
        /// </summary>
        public Nullable<long> OcOrderId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.oc.order.ap.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("oc_order_id", this.OcOrderId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("oc_order_id", this.OcOrderId);
        }

        #endregion
    }
}
