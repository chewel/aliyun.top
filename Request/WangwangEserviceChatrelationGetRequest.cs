using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.wangwang.eservice.chatrelation.get
    /// </summary>
    public class WangwangEserviceChatrelationGetRequest : BaseTopRequest<Top.Api.Response.WangwangEserviceChatrelationGetResponse>
    {
        /// <summary>
        /// 请求参数
        /// </summary>
        public string ChatRelationRequest { get; set; }

        public ChatRelationRequestDomain ChatRelationRequest_ { set { this.ChatRelationRequest = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.wangwang.eservice.chatrelation.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("chat_relation_request", this.ChatRelationRequest);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("chat_relation_request", this.ChatRelationRequest);
        }

	/// <summary>
/// ChatRelationRequestDomain Data Structure.
/// </summary>
[Serializable]

public class ChatRelationRequestDomain : TopObject
{
	        /// <summary>
	        /// 查询起始时间（精度到天）
	        /// </summary>
	        [XmlElement("beg")]
	        public Nullable<DateTime> Beg { get; set; }
	
	        /// <summary>
	        /// 查询条数
	        /// </summary>
	        [XmlElement("count")]
	        public Nullable<long> Count { get; set; }
	
	        /// <summary>
	        /// 查询结束时间（精度到天）
	        /// </summary>
	        [XmlElement("end")]
	        public Nullable<DateTime> End { get; set; }
	
	        /// <summary>
	        /// 翻页查询起始key
	        /// </summary>
	        [XmlElement("page_beg")]
	        public string PageBeg { get; set; }
	
	        /// <summary>
	        /// 翻页查询结束key
	        /// </summary>
	        [XmlElement("page_end")]
	        public string PageEnd { get; set; }
	
	        /// <summary>
	        /// 查询账号
	        /// </summary>
	        [XmlElement("uid")]
	        public string Uid { get; set; }
}

        #endregion
    }
}
