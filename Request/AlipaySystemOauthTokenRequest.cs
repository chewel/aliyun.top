using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: alipay.system.oauth.token
    /// </summary>
    public class AlipaySystemOauthTokenRequest : BaseTopRequest<Top.Api.Response.AlipaySystemOauthTokenResponse>
    {
        /// <summary>
        /// 授权码，用户对应用授权后得到。
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 获取访问令牌的类型，authorization_code表示用授权码换，refresh_token表示用刷新令牌来换。
        /// </summary>
        public string GrantType { get; set; }

        /// <summary>
        /// 刷新令牌，上次换取访问令牌是得到。
        /// </summary>
        public string RefreshToken { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "alipay.system.oauth.token";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("code", this.Code);
            parameters.Add("grant_type", this.GrantType);
            parameters.Add("refresh_token", this.RefreshToken);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateMaxLength("code", this.Code, 40);
            RequestValidator.ValidateRequired("grant_type", this.GrantType);
            RequestValidator.ValidateMaxLength("grant_type", this.GrantType, 20);
            RequestValidator.ValidateMaxLength("refresh_token", this.RefreshToken, 40);
        }

        #endregion
    }
}
