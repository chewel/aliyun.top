using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: alipay.user.account.get
    /// </summary>
    public class AlipayUserAccountGetRequest : BaseTopRequest<Top.Api.Response.AlipayUserAccountGetResponse>
    {
        #region ITopRequest Members

        public override string GetApiName()
        {
            return "alipay.user.account.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
