using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.item.edu.update.outline
    /// </summary>
    public class TmallItemEduUpdateOutlineRequest : BaseTopRequest<Top.Api.Response.TmallItemEduUpdateOutlineResponse>
    {
        /// <summary>
        /// 宝贝Id
        /// </summary>
        public Nullable<long> ItemId { get; set; }

        /// <summary>
        /// 课程大纲json
        /// </summary>
        public string OutlineInfo { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.item.edu.update.outline";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("item_id", this.ItemId);
            parameters.Add("outline_info", this.OutlineInfo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("item_id", this.ItemId);
            RequestValidator.ValidateRequired("outline_info", this.OutlineInfo);
        }

        #endregion
    }
}
