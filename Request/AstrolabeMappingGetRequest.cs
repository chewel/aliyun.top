using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.astrolabe.mapping.get
    /// </summary>
    public class AstrolabeMappingGetRequest : BaseTopRequest<Top.Api.Response.AstrolabeMappingGetResponse>
    {
        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.astrolabe.mapping.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
