using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.oc.ap.contractsigned.get
    /// </summary>
    public class OcApContractsignedGetRequest : BaseTopRequest<Top.Api.Response.OcApContractsignedGetResponse>
    {
        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.oc.ap.contractsigned.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
