using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: alipay.point.budget.get
    /// </summary>
    public class AlipayPointBudgetGetRequest : BaseTopRequest<Top.Api.Response.AlipayPointBudgetGetResponse>
    {
        /// <summary>
        /// 支付宝给用户的授权。如果没有top的授权，这个字段是必填项
        /// </summary>
        public string AuthToken { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "alipay.point.budget.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("auth_token", this.AuthToken);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
