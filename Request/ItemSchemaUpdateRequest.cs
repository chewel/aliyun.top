using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.item.schema.update
    /// </summary>
    public class ItemSchemaUpdateRequest : BaseTopRequest<Top.Api.Response.ItemSchemaUpdateResponse>
    {
        /// <summary>
        /// 如果重新选择类目后，传入重新选择的叶子类目id
        /// </summary>
        public Nullable<long> CategoryId { get; set; }

        /// <summary>
        /// 是否增量更新。为true只改xml_data里传入的值。为false时，将根据xml_data的数据对原始宝贝数据进行全量覆盖,这个参数暂时不支持
        /// </summary>
        public Nullable<bool> Incremental { get; set; }

        /// <summary>
        /// 编辑商品的商品id
        /// </summary>
        public Nullable<long> ItemId { get; set; }

        /// <summary>
        /// 编辑商品时候的修改内容
        /// </summary>
        public string XmlData { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.item.schema.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("category_id", this.CategoryId);
            parameters.Add("incremental", this.Incremental);
            parameters.Add("item_id", this.ItemId);
            parameters.Add("xml_data", this.XmlData);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("item_id", this.ItemId);
            RequestValidator.ValidateRequired("xml_data", this.XmlData);
        }

        #endregion
    }
}
