using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.wangwang.abstract.initialize
    /// </summary>
    public class WangwangAbstractInitializeRequest : BaseTopRequest<Top.Api.Response.WangwangAbstractInitializeResponse>
    {
        /// <summary>
        /// 传入参数的字符集
        /// </summary>
        public string Charset { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.wangwang.abstract.initialize";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("charset", this.Charset);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
