using System;
using System.Xml.Serialization;

namespace Top.Api.Domain
{
    /// <summary>
    /// ChildOcOrder Data Structure.
    /// </summary>
    [Serializable]
    public class ChildOcOrder : TopObject
    {
        /// <summary>
        /// 分帐金额
        /// </summary>
        [XmlElement("allocate_payment_amount")]
        public long AllocatePaymentAmount { get; set; }

        /// <summary>
        /// 分账参与方
        /// </summary>
        [XmlElement("allocate_payment_participants")]
        public string AllocatePaymentParticipants { get; set; }

        /// <summary>
        /// 分账规则对象
        /// </summary>
        [XmlElement("allocate_payment_rule")]
        public Top.Api.Domain.AllocatePaymentRule AllocatePaymentRule { get; set; }

        /// <summary>
        /// 分账ruleId,调用创建规则获取
        /// </summary>
        [XmlElement("allocate_payment_rule_id")]
        public long AllocatePaymentRuleId { get; set; }

        /// <summary>
        /// 分账执行时间
        /// </summary>
        [XmlElement("ap_status_modify_time")]
        public string ApStatusModifyTime { get; set; }

        /// <summary>
        /// 0:初始状态(无状态)1:分账成功 -1:分账失败2:分账已经回调
        /// </summary>
        [XmlElement("divide_status")]
        public long DivideStatus { get; set; }

        /// <summary>
        /// 子订单id
        /// </summary>
        [XmlElement("id")]
        public long Id { get; set; }

        /// <summary>
        /// 主扣款账号
        /// </summary>
        [XmlElement("main_allocation_payment_acount")]
        public long MainAllocationPaymentAcount { get; set; }

        /// <summary>
        /// 父订单id
        /// </summary>
        [XmlElement("parent_id")]
        public long ParentId { get; set; }

        /// <summary>
        /// 订单来源渠道TB,或者TMALL
        /// </summary>
        [XmlElement("source_trade_channel")]
        public string SourceTradeChannel { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        [XmlElement("source_trade_no")]
        public string SourceTradeNo { get; set; }
    }
}
