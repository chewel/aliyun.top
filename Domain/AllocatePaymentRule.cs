using System;
using System.Xml.Serialization;

namespace Top.Api.Domain
{
    /// <summary>
    /// AllocatePaymentRule Data Structure.
    /// </summary>
    [Serializable]
    public class AllocatePaymentRule : TopObject
    {
        /// <summary>
        /// 分账规则的详细描述
        /// </summary>
        [XmlElement("content")]
        public string Content { get; set; }

        /// <summary>
        /// 分账方式(byPercent,byAmount)
        /// </summary>
        [XmlElement("division_rule")]
        public string DivisionRule { get; set; }

        /// <summary>
        /// 分账的时间类型(month,day,now)
        /// </summary>
        [XmlElement("excution_period")]
        public string ExcutionPeriod { get; set; }

        /// <summary>
        /// 分账规则的id
        /// </summary>
        [XmlElement("id")]
        public long Id { get; set; }
    }
}
