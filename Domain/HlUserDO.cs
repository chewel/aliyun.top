using System;
using System.Xml.Serialization;

namespace Top.Api.Domain
{
    /// <summary>
    /// HlUserDO Data Structure.
    /// </summary>
    [Serializable]
    public class HlUserDO : TopObject
    {
        /// <summary>
        /// 回流信息是否开通买家端展示
        /// </summary>
        [XmlElement("open_for_buyer")]
        public string OpenForBuyer { get; set; }

        /// <summary>
        /// 如果为空，则默认是X_TO_SYSTEM,X_WAIT_ALLOCATION,X_OUT_WAREHOUSE
        /// </summary>
        [XmlElement("open_nodes")]
        public string OpenNodes { get; set; }
    }
}
