using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Domain
{
    /// <summary>
    /// OcOrder Data Structure.
    /// </summary>
    [Serializable]
    public class OcOrder : TopObject
    {
        /// <summary>
        /// 分账总金额
        /// </summary>
        [XmlElement("allocate_payment_amount")]
        public long AllocatePaymentAmount { get; set; }

        /// <summary>
        /// 表示分账参与方
        /// </summary>
        [XmlElement("allocate_payment_participants")]
        public string AllocatePaymentParticipants { get; set; }

        /// <summary>
        /// 分账规则
        /// </summary>
        [XmlElement("allocate_payment_rule")]
        public Top.Api.Domain.AllocatePaymentRule AllocatePaymentRule { get; set; }

        /// <summary>
        /// 分账规则id
        /// </summary>
        [XmlElement("allocate_payment_rule_id")]
        public long AllocatePaymentRuleId { get; set; }

        /// <summary>
        /// 分账执行时间
        /// </summary>
        [XmlElement("ap_status_modify_time")]
        public string ApStatusModifyTime { get; set; }

        /// <summary>
        /// 0:初始状态(无状态)1:分赃成功 -1:分赃失败2:分账已经回调
        /// </summary>
        [XmlElement("divide_status")]
        public long DivideStatus { get; set; }

        /// <summary>
        /// OC订单号
        /// </summary>
        [XmlElement("id")]
        public long Id { get; set; }

        /// <summary>
        /// 淘宝账户id
        /// </summary>
        [XmlElement("main_allocation_payment_acount")]
        public long MainAllocationPaymentAcount { get; set; }

        /// <summary>
        /// 子订单集合
        /// </summary>
        [XmlArray("order_lines")]
        [XmlArrayItem("child_oc_order")]
        public List<Top.Api.Domain.ChildOcOrder> OrderLines { get; set; }

        /// <summary>
        /// 来源渠道
        /// </summary>
        [XmlElement("source_trade_channel")]
        public string SourceTradeChannel { get; set; }

        /// <summary>
        /// 源订单号
        /// </summary>
        [XmlElement("source_trade_no")]
        public string SourceTradeNo { get; set; }
    }
}
