﻿using System;
using System.Runtime.Serialization;

namespace Top.Api
{
    /// <summary>
    /// TOP客户端异常。
    /// </summary>
    public class TopException : Exception
    {
        public TopException()
            : base()
        {
        }

        public TopException(string message)
            : base(message)
        {
        }

        protected TopException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public TopException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public TopException(string errorCode, string errorMsg)
            : base(errorCode + ":" + errorMsg)
        {
            this.ErrorCode = errorCode;
            this.ErrorMsg = errorMsg;
        }

        public TopException(string errorCode, string errorMsg, string subErrorCode, string subErrorMsg)
            : base(errorCode + ":" + errorMsg + ":" + subErrorCode + ":" + subErrorMsg)
        {
            this.ErrorCode = errorCode;
            this.ErrorMsg = errorMsg;
            this.SubErrorCode = subErrorCode;
            this.SubErrorMsg = subErrorMsg;
        }

        public string ErrorCode { get; }

        public string ErrorMsg { get; }

        public string SubErrorCode { get; }

        public string SubErrorMsg { get; }
    }
}
