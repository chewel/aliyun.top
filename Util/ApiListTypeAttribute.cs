﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Top.Api.Util
{
    public class ApiListTypeAttribute : Attribute
    {
        public string Value { get; set; }

        public ApiListTypeAttribute(string value)
        {
            this.Value = value;
        }
    }
}
