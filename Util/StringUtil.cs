﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Top.Api.Util
{
    public abstract class StringUtil
    {
        private const string pattern = @"^\d*$";
        private static readonly Regex REG_CIDR = new Regex("^(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})/(\\d{1,2})$");


        public static bool IsNumeric(string obj)
        {
            if (obj == null)
            {
                return false;
            }
            var chars = obj.ToCharArray();
            var length = chars.Length;
            if (length < 1)
                return false;

            var i = 0;
            if (length > 1 && chars[0] == '-')
                i = 1;

            for (; i < length; i++)
            {
                if (!char.IsDigit(chars[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsDigits(string value)
        {
            return !string.IsNullOrEmpty(value) && Regex.IsMatch(value, pattern);
        }

        public static string[] Split(string data, char separatorChar)
        {
            return string.IsNullOrEmpty(data) ? null : data.Split(new char[] { separatorChar }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string ToCamelStyle(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return name;
            }

            var chars = name.ToCharArray();
            var buf = new StringBuilder(chars.Length);
            for (var i = 0; i < chars.Length; i++)
            {
                buf.Append(i == 0 ? char.ToLower(chars[i]) : chars[i]);
            }
            return buf.ToString();
        }

        public static string ToUnderlineStyle(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return name;
            }

            var buf = new StringBuilder();
            for (var i = 0; i < name.Length; i++)
            {
                var c = name[i];
                if (char.IsUpper(c))
                {
                    if (i > 0)
                    {
                        buf.Append("_");
                    }
                    buf.Append(char.ToLower(c));
                }
                else
                {
                    buf.Append(c);
                }
            }
            return buf.ToString();
        }

        public static string EscapeXml(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            var buf = new StringBuilder();
            var chars = value.ToCharArray();

            foreach (var c in chars)
            {
                switch (c)
                {
                    case '<':
                        buf.Append("&lt;");
                        break;
                    case '>':
                        buf.Append("&gt;");
                        break;
                    case '\'':
                        buf.Append("&apos;");
                        break;
                    case '&':
                        buf.Append("&amp;");
                        break;
                    case '"':
                        buf.Append("&quot;");
                        break;
                    default:
                        if ((c == 0x9) || (c == 0xA) || (c == 0xD) || ((c >= 0x20) && (c <= 0xD7FF))
                            || ((c >= 0xE000) && (c <= 0xFFFD)) || ((c >= 0x10000) && (c <= 0x10FFFF)))
                        {
                            buf.Append(c);
                        }
                        break;
                }
            }

            return buf.ToString();
        }

        public static string FormatDateTime(DateTime dateTime)
        {
            return dateTime.ToString(Constants.DATE_TIME_FORMAT);
        }

        public static bool IsIpInRange(string ipAddr, string cidrAddr)
        {
            var match = REG_CIDR.Match(cidrAddr);
            if (!match.Success)
            {
                throw new TopException("Invalid CIDR address: " + cidrAddr);
            }

            var minIpParts = new int[4];
            var maxIpParts = new int[4];
            var ipParts = match.Groups[1].Value.Split(new string[1] { "." }, StringSplitOptions.None);
            var intMask = int.Parse(match.Groups[2].Value);

            for (var i = 0; i < ipParts.Length; i++)
            {
                var ipPart = int.Parse(ipParts[i]);
                if (intMask > 8)
                {
                    minIpParts[i] = ipPart;
                    maxIpParts[i] = ipPart;
                    intMask -= 8;
                }
                else if (intMask > 0)
                {
                    minIpParts[i] = ipPart >> intMask;
                    maxIpParts[i] = ipPart | (0xFF >> intMask);
                    intMask = 0;
                }
                else
                {
                    minIpParts[i] = 1;
                    maxIpParts[i] = 0xFF - 1;
                }
            }

            var realIpParts = ipAddr.Split(new string[1] { "." }, StringSplitOptions.None);
            return !realIpParts.Select(int.Parse).Where((realIp, i) => realIp < minIpParts[i] || realIp > maxIpParts[i]).Any();
        }
    }
}
