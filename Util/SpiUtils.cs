﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Microsoft.AspNetCore.Http;

namespace Top.Api.Util
{
    /// <summary>
    /// SPI请求校验结果。
    /// </summary>
    public class CheckResult
    {
        public bool Success { get; set; }

        public string Body { get; set; }
    }

    /// <summary>
    /// SPI服务提供方工具类。
    /// </summary>
    public class SpiUtils
    {
        private const string TOP_SIGN_LIST = "top-sign-list";
        private static readonly string[] HEADER_FIELDS_IP = {"X-Real-IP", "X-Forwarded-For", "Proxy-Client-IP",
        "WL-Proxy-Client-IP", "HTTP_CLIENT_IP", "HTTP_X_FORWARDED_FOR"};

        /// <summary>
        /// 校验SPI请求签名，不支持带上传文件的HTTP请求。
        /// </summary>
        /// <param name="request">HttpRequest对象实例</param>
        /// <param name="secret">APP密钥</param>
        /// <returns>校验结果</returns>
        public static CheckResult CheckSign(HttpRequest request, string secret)
        {
            var result = new CheckResult();
            var ctype = request.ContentType;
            if (ctype.StartsWith(Constants.CTYPE_APP_JSON) || ctype.StartsWith(Constants.CTYPE_TEXT_XML) || ctype.StartsWith(Constants.CTYPE_TEXT_PLAIN))
            {
                result.Body = GetStreamAsString(request, GetRequestCharset(ctype));
                result.Success = CheckSignInternal(request, result.Body, secret);
            }
            else if (ctype.StartsWith(Constants.CTYPE_FORM_DATA))
            {
                result.Success = CheckSignInternal(request, null, secret);
            }
            else
            {
                throw new TopException("Unspported SPI request");
            }
            return result;
        }

        /// <summary>
        /// 校验SPI请求签名，适用于Content-Type为application/x-www-form-urlencoded或multipart/form-data的GET或POST请求。
        /// </summary>
        /// <param name="request">请求对象</param>
        /// <param name="secret">app对应的secret</param>
        /// <returns>true：校验通过；false：校验不通过</returns>
        public static bool CheckSign4FormRequest(HttpRequest request, string secret)
        {
            return CheckSignInternal(request, null, secret);
        }

        /// <summary>
        /// 校验SPI请求签名，适用于Content-Type为text/xml或text/json的POST请求。
        /// </summary>
        /// <param name="request">请求对象</param>
        /// <param name="body">请求体的文本内容</param>
        /// <param name="secret">app对应的secret</param>
        /// <returns>true：校验通过；false：校验不通过</returns>
        public static bool CheckSign4TextRequest(HttpRequest request, string body, string secret)
        {
            return CheckSignInternal(request, body, secret);
        }

        private static bool CheckSignInternal(HttpRequest request, string body, string secret)
        {
            IDictionary<string, string> parameters = new SortedDictionary<string, string>(StringComparer.Ordinal);
            var charset = GetRequestCharset(request.ContentType);

            // 1. 获取header参数
            AddAll(parameters, GetHeaderMap(request, charset));

            // 2. 获取url参数
            var queryMap = GetQueryMap(request, charset);
            AddAll(parameters, queryMap);

            // 3. 获取form参数
            AddAll(parameters, GetFormMap(request));

            // 4. 生成签名并校验
            string remoteSign = null;
            if (queryMap.ContainsKey(Constants.SIGN))
            {
                remoteSign = queryMap[Constants.SIGN];
            }
            var localSign = Sign(parameters, body, secret, charset);
            return localSign.Equals(remoteSign);
        }

        private static void AddAll(IDictionary<string, string> dest, IDictionary<string, string> from)
        {
            if (from != null && from.Count > 0)
            {
                using (var em = from.GetEnumerator())
                {
                    while (em.MoveNext())
                    {
                        var kvp = em.Current;
                        dest.Add(kvp.Key, kvp.Value);
                    }
                } 
            }
        }

        /// <summary>
        /// 签名规则：hex(md5(secret+sorted(header_params+url_params+form_params)+body)+secret)
        /// </summary>
        private static string Sign(IDictionary<string, string> parameters, string body, string secret, string charset)
        {
            var query = new StringBuilder(secret);
            using (var em = parameters.GetEnumerator())
            {
                while (em.MoveNext())
                {
                    var key = em.Current.Key;
                    if (!Constants.SIGN.Equals(key))
                    {
                        var value = em.Current.Value;
                        query.Append(key).Append(value);
                    }
                }
            }

            // 第1步：把所有参数名和参数值串在一起
                
            if (body != null)
            {
                query.Append(body);
            }

            query.Append(secret);

            // 第2步：使用MD5加密
            var md5 = MD5.Create();
            var bytes = md5.ComputeHash(Encoding.GetEncoding(charset).GetBytes(query.ToString()));

            // 第3步：把二进制转化为大写的十六进制
            var result = new StringBuilder();
            foreach (var t in bytes)
            {
                result.Append(t.ToString("X2"));
            }

            return result.ToString();
        }

        private static string GetRequestCharset(string ctype)
        {
            var charset = "utf-8";
            if (!string.IsNullOrEmpty(ctype))
            {
                var entires = ctype.Split(';');
                foreach (var entry in entires)
                {
                    var _entry = entry.Trim();
                    if (_entry.StartsWith("charset"))
                    {
                        var pair = _entry.Split('=');
                        if (pair.Length == 2)
                        {
                            if (!string.IsNullOrEmpty(pair[1]))
                            {
                                charset = pair[1].Trim();
                            }
                        }
                        break;
                    }
                }
            }
            return charset;
        }

        public static Dictionary<string, string> GetHeaderMap(HttpRequest request, string charset)
        {
            var headerMap = new Dictionary<string, string>();
            string signList = request.Headers[TOP_SIGN_LIST];
            if (!string.IsNullOrEmpty(signList))
            {
                var keys = signList.Split(',');
                foreach (var key in keys)
                {
                    string value = request.Headers[key];
                    headerMap.Add(key,
                        string.IsNullOrEmpty(value) ? "" : HttpUtility.UrlDecode(value, Encoding.GetEncoding(charset)));
                }
            }
            return headerMap;
        }

        public static Dictionary<string, string> GetQueryMap(HttpRequest request, string charset)
        {
            var queryMap = new Dictionary<string, string>();
            var queryString = request.QueryString.Value;
            if (!string.IsNullOrEmpty(queryString))
            {
                queryString = queryString.Substring(1); // 忽略?号
                var parameters = queryString.Split('&');
                foreach (var parameter in parameters)
                {
                    var kv = parameter.Split('=');
                    switch (kv.Length)
                    {
                        case 2:
                        {
                            var key = HttpUtility.UrlDecode(kv[0], Encoding.GetEncoding(charset));
                            var value = HttpUtility.UrlDecode(kv[1], Encoding.GetEncoding(charset));
                            queryMap.Add(key, value);
                            break;
                        }
                        case 1:
                        {
                            var key = HttpUtility.UrlDecode(kv[0], Encoding.GetEncoding(charset));
                            queryMap.Add(key, "");
                            break;
                        }
                    }
                }
            }
            return queryMap;
        }

        public static Dictionary<string, string> GetFormMap(HttpRequest request)
        {
            var formMap = new Dictionary<string, string>();
            var form = request.Form;
            var keys = form.Keys;
            foreach (var key in keys)
            {
                string value = request.Form[key];
                formMap.Add(key, string.IsNullOrEmpty(value) ? "" : value);
            }
            return formMap;
        }

        public static string GetStreamAsString(HttpRequest request, string charset)
        {
            Stream stream = null;
            StreamReader reader = null;

            try
            {
                // 以字符流的方式读取HTTP请求体
                stream = request.Body;
                reader = new StreamReader(stream, Encoding.GetEncoding(charset));
                return reader.ReadToEnd();
            }
            finally
            {
                // 释放资源
                reader?.Close();
                stream?.Close();
            }
        }

        /// <summary>
        /// 检查SPI请求到达服务器端是否已经超过指定的分钟数，如果超过则拒绝请求。
        /// </summary>
        /// <returns>true代表不超过，false代表超过。</returns>
        public static bool CheckTimestamp(HttpRequest request, int minutes)
        {
            string ts = request.Query[Constants.TIMESTAMP];
            if (!string.IsNullOrEmpty(ts))
            {
                var remote = DateTime.ParseExact(ts, Constants.DATE_TIME_FORMAT, null);
                var local = DateTime.Now;
                return remote.AddMinutes(minutes).CompareTo(local) > 0;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 检查发起SPI请求的来源IP是否是TOP机房的出口IP。
        /// </summary>
        /// <param name="request">HTTP请求对象</param>
        /// <param name="topIpList">TOP网关IP出口地址段列表，通过taobao.top.ipout.get获得</param>
        /// <returns>true表达IP来源合法，false代表IP来源不合法</returns>
        public static bool CheckRemoteIp(HttpRequest request, List<string> topIpList)
        {
            var ip = request.Host.Host;
            foreach (var ipHeader in HEADER_FIELDS_IP)
            {
                string realIp = request.Headers[ipHeader];
                if (!string.IsNullOrEmpty(realIp) && !"unknown".Equals(realIp))
                {
                    ip = realIp;
                    break;
                }
            }

            return topIpList != null && topIpList.Any(topIp => StringUtil.IsIpInRange(ip, topIp));
        }
    }
}
