﻿using System;
using System.Collections.Generic;

namespace Top.Api
{
    /// <summary>
    /// 符合TOP习惯的纯字符串字典结构。
    /// </summary>
    public class TopDictionary : Dictionary<string, string>
    {
        public TopDictionary() { }

        public TopDictionary(IDictionary<string, string> dictionary)
            : base(dictionary)
        { }

        /// <summary>
        /// 添加一个新的键值对。空键或者空值的键值对将会被忽略。
        /// </summary>
        /// <param name="key">键名称</param>
        /// <param name="value">键对应的值，目前支持：string, int, long, double, bool, DateTime类型</param>
        public void Add(string key, object value)
        {
            string strValue;

            switch (value)
            {
                case null:
                    strValue = null;
                    break;
                case string _:
                    strValue = (string)value;
                    break;
                default:
                    if (value is DateTime?)
                    {
                        var dateTime = value as DateTime?;
                        strValue = dateTime.Value.ToString(Constants.DATE_TIME_FORMAT);
                    }
                    else if (value is int?)
                    {
                        strValue = (value as int?).Value.ToString();
                    }
                    else if (value is long?)
                    {
                        strValue = (value as long?).Value.ToString();
                    }
                    else if (value is double?)
                    {
                        strValue = (value as double?).Value.ToString();
                    }
                    else if (value is bool?)
                    {
                        strValue = (value as bool?).Value.ToString().ToLower();
                    }
                    else
                    {
                        strValue = value.ToString();
                    }
                    break;
            }

            this.Add(key, strValue);
        }

        public new void Add(string key, string value)
        {
            if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
            {
                base[key] = value;
            }
        }

        public void AddAll(IDictionary<string, string> dict)
        {
            if (dict != null && dict.Count > 0)
            {
                using (var kvps = dict.GetEnumerator())
                {
                    while (kvps.MoveNext())
                    {
                        var kvp = kvps.Current;
                        Add(kvp.Key, kvp.Value);
                    }
                }
                    
            }
        }
    }
}
